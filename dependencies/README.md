# Сборка и публикация app

cd app
docker build --pull --compress --tag bronner/app-base:latest .
docker push bronner/app-base

# Сборка и публикация php

cd php-fpm
docker build --pull --compress --tag bronner/php-fpm-base:latest .
docker push bronner/php-fpm-base